#!/bin/bash

STYLESHEET="wizdom"
STYLESDIR="stylesheets"
IMAGESDIR="resources"
EXPORTDIR="public"

# compile sassy stylesheets (committed in ./sass folder)
# into CSS stylesheets which will be created in $STYLESDIR
# tose locations are set in ./config.rb
compass compile
# don't forget to copy resources, too
mkdir -p ${EXPORTDIR}
cp -r resources ${EXPORTDIR}/

###################
# COURSE MATERIAL #
###################
# generate .html page in output folder
asciidoctor core.adoc \
         -a stylesheet="${STYLESHEET}.css"    \
         -a stylesdir="${STYLESDIR}"       \
         -o ${EXPORTDIR}/index.html
# generate .pdf document in output folder
wkhtmltopdf --enable-internal-links --enable-external-links \
            ${EXPORTDIR}/index.html \
            ${EXPORTDIR}/pdf
